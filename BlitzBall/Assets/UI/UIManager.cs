﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    [Header("Focus")]
    public EventSystem nav;
    public EventSystem navP2;
    public GameObject lastbutton;
    public GameObject lastbuttonP2;

    [Header("Screen")]
    public List<GameObject> Screens;

    [Header("Button Group")]
    public GameObject startObj;
    public List<GameObject> ButtonGrp;
    int selected = 0;

    [Header("Audio")]
    public AudioMixer audioMixer;
    float lastVolume;
    float lastVolumeMusic;
    float lastVolumeSFX;
    public AudioSource SFX;
    public AudioSource Music;

    [Header("Clip")]
    public AudioClip select;
    public AnimationCurve soundMap;

    void Start()
    {
        lastbutton = nav.firstSelectedGameObject;
        lastbuttonP2 = navP2.firstSelectedGameObject;
        audioMixer.SetFloat("VolumeMusic", soundMap.Evaluate(1- 0.75f) * -80);
        audioMixer.SetFloat("VolumeSFX", soundMap.Evaluate(1- 0.75f) * -80);
        SFX = GameObject.Find("/AudioSource/SFX").GetComponent<AudioSource>();
        Music = GameObject.Find("/AudioSource/Music").GetComponent<AudioSource>();
    }

    void truc (){
        //nav.currentSelectedGameObject.GetComponent<MyButton>().isOn = true;
        SelectedPlayer(true);
    }
    void Update()
    {
        Debug.Log(selected + "selected");
        if(nav.currentSelectedGameObject != lastbutton)
        {
            SFX.clip = select;
            SFX.Play();
        }
        /*if(navP2.currentSelectedGameObject)
        {
            if(navP2.currentSelectedGameObject != lastbuttonP2)
            {
                SFX.clip = select;
                SFX.Play();
            }
        }*/
        if(selected == 2)
        {
            startObj.SetActive(true);   
            nav.gameObject.SetActive(false);
            navP2.gameObject.SetActive(false);
            if(Input.GetKeyDown(KeyCode.Joystick1Button1))
            {
                nav.currentSelectedGameObject.GetComponent<MyButton>().isOn = false;
                SelectedPlayer(false);
                startObj.SetActive(false);
                nav.gameObject.SetActive(true);
                navP2.gameObject.SetActive(true);
                Debug.Log(selected + "selected");
                Invoke("truc", 0.5f);
            }     

            if(Input.GetKeyDown(KeyCode.Joystick2Button1))
            {
                nav.currentSelectedGameObject.GetComponent<MyButton>().isOn = false;
                SelectedPlayer(false);
                startObj.SetActive(false);
                nav.gameObject.SetActive(true);
                navP2.gameObject.SetActive(true);
                Debug.Log(selected + "selected");
                Invoke("truc", 0.5f);
            }     

            if(Input.GetKey(KeyCode.Joystick1Button7))
            {
                SceneManager.LoadScene("1v1Classic");
            }
        }
    }

    void LateUpdate()
    {
        
        if(nav.currentSelectedGameObject != lastbutton && !SFX.isPlaying)
        {
            // SFX.Stop();
            SFX.clip = select;
            SFX.Play();
        }

        if(navP2.currentSelectedGameObject != lastbuttonP2 && !SFX.isPlaying)
        {
            // SFX.Stop();
            SFX.clip = select;
            SFX.Play();
        }

        if(nav.currentSelectedGameObject)
        {
            lastbutton = nav.currentSelectedGameObject;
        }
        else
        {
            nav.SetSelectedGameObject(lastbutton);
        }

        if(navP2.currentSelectedGameObject)
        {
            lastbuttonP2 = navP2.currentSelectedGameObject;
        }
        else
        {
            navP2.SetSelectedGameObject(lastbuttonP2);
        }
    }
    public void GoToScreen(GameObject obj)
    {
        foreach(GameObject go in Screens)
            if(go!=obj)
                go.SetActive(false);

        obj.SetActive(true);
    }

    public void EnableButtonGrp(GameObject obj)
    {
        foreach (GameObject go in ButtonGrp)
            if(go!=obj)
                go.SetActive(false);

        obj.SetActive(true);
    }

    public void SelectedPlayer(bool select) {
        if(select) 
        {
            selected++;
        }
        else 
        {
            selected--;
        }
    }
    public void SetVolume(float volume)
    {        
        audioMixer.SetFloat("VolumeMaster", soundMap.Evaluate(1- volume)*-80);
        lastVolume = volume;
    }
    public void SetVolumeMusic(float volume)
    {
        audioMixer.SetFloat("VolumeMusic", soundMap.Evaluate(1- volume) * -80);
        lastVolumeMusic = volume;
    }
    public void SetVolumeSFX(float volume)
    {
        audioMixer.SetFloat("VolumeSFX", soundMap.Evaluate(1- volume) * -80);
        lastVolumeSFX = volume;
        SFX.Play();
    }
    public void Mute(bool mute)
    {
        if (mute)
        {
            audioMixer.SetFloat("VolumeMaster", -80);
            audioMixer.SetFloat("VolumeMusic", -80);
            audioMixer.SetFloat("VolumeSFX", -80);
        }
        else
        {
            audioMixer.SetFloat("VolumeMaster", lastVolume);
            audioMixer.SetFloat("VolumeMusic", lastVolumeMusic);
            audioMixer.SetFloat("VolumeSFX", lastVolumeSFX);
        }
    }
}