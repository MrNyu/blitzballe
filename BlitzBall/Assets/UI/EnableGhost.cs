﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace ActionCode2D.Renderers
{
    public class EnableGhost : MonoBehaviour
    {
        public SpriteGhostTrailRenderer trail;
        public void EnableTrail(bool enable)
        {
            if(enable)
            {
                trail.enabled = true;
            }
            else
            {
                trail.enabled = false;
            }
        }
    }
}
