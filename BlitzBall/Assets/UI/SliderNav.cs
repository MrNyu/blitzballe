﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SliderNav : MonoBehaviour
{
    public EventSystem nav;
    bool selected = false;
    bool down;
    public float incremant = 1;
    Transform handle;
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Joystick1Button0) && nav.gameObject.activeInHierarchy &&nav.currentSelectedGameObject.CompareTag("Slider"))
        {           
            if(nav.currentSelectedGameObject.GetComponent<hasAHandle>())
                handle = nav.currentSelectedGameObject.GetComponent<hasAHandle>().Handle;

            handle.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            nav.gameObject.SetActive(false);
            selected = true;
        }
        else if(Input.GetKeyDown(KeyCode.Joystick1Button0) && !nav.gameObject.activeInHierarchy &&nav.currentSelectedGameObject.CompareTag("Slider"))
        {            
            if(nav.currentSelectedGameObject.GetComponent<hasAHandle>())
                handle = nav.currentSelectedGameObject.GetComponent<hasAHandle>().Handle;
                
            handle.localScale = new Vector3(2f, 2f, 2f);
            nav.gameObject.SetActive(true);
            selected = false;
        }

        if (selected && Input.GetAxis("Horizontal") < 0.2 && selected && Input.GetAxis("Horizontal") > -0.2)
            down = false;

        if(selected && Input.GetAxis("Horizontal")>0.2 && !down)
        {
            nav.currentSelectedGameObject.GetComponent<Slider>().value += incremant;
            down = true;
        }

        if(selected && Input.GetAxis("Horizontal") < -0.2 && !down)
        {
            nav.currentSelectedGameObject.GetComponent<Slider>().value -= incremant;
            down = true;
        }
    }
}
