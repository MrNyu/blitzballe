﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HighlightButton : MonoBehaviour
{
    public EventSystem nav;
    public RectTransform lighter;
    
    public float offset = 0;
    float xPos;
    float yPos;

    void Update()
    {       
        RectTransform rt = (RectTransform)nav.currentSelectedGameObject.transform;
        yPos = rt.localPosition.y;
        xPos = rt.localPosition.x - rt.rect.width/2 - offset * ((RectTransform)transform).rect.width;
        lighter.localPosition = new Vector3(xPos, yPos, 0);
        Debug.Log(nav.currentSelectedGameObject);
    }
}