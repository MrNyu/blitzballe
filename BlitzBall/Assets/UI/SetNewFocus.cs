﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SetNewFocus : MonoBehaviour
{
    public EventSystem nav;
    public GameObject focus;

    void OnEnable()
    {
        nav.SetSelectedGameObject(focus);
    }
}
