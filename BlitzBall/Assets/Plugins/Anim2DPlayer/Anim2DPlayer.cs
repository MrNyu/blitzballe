﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Anim2D;

[RequireComponent(typeof(Animator))]
public class Anim2DPlayer : MonoBehaviour {
	// Components
	public Animator animator;
	SpriteRenderer spriteRenderer;
	public bool playFirstOnAwake = true;

	// Auto direction
	public enum AutoDirectionType { Manual, UseTransformXY, UseTransformXZ, UseInputAxes }
	public AutoDirectionType autoDirection = AutoDirectionType.UseTransformXY;

	// State List
	[SerializeField]
	public List<AnimState> states = new List<AnimState>();
	Dictionary<string, AnimState> stateDictionnary = new Dictionary<string, AnimState> ();
	float currStateTime;

	// Current Sate
	AnimState currentState;
	int lastStateHash;
	bool canExit = true;
	string nextToCheck;

	// Current clip
	AnimClip currentClip;
	int lastClipHash;
	Vector2 currDirection;

	// Positions (for auto direction)
	Vector3 currPosition;
	Vector3 lastPosition;

	// Used to flip (or not)
	bool lastFlipScale = false;
	bool lastFlipAngle = false;

	void Awake () {
		GrabComponents ();
		UpdateDictionnary ();
		currPosition = transform.position;
		lastPosition = currPosition;
		if (playFirstOnAwake && stateDictionnary.Count > 0) {
			SetState (0);
		}
	}

	void Update () {
		currStateTime = animator.GetCurrentAnimatorStateInfo (0).normalizedTime;
		ExitOnTime ();
	}

	void FixedUpdate () {
		AutomaticDirection ();
	}

	public float GetCurrentTime () {
		return currStateTime;
	}

	public AnimClip GetCurrentClip () {
		return currentClip;
	}

	public AnimState GetCurrentState () {
		return currentState;
	}

	public Vector2 GetCurrentDirection () {
		return currentState.GetCurrentDirection ();
	}

	/// <summary>
	/// Changes the direction on the current playing state.
	/// </summary>
	/// <param name="direction">Direction.</param>
	public void SetDirection (AnimDirection direction) {
		Vector2 _dir = Anim2D.AnimState.AnimDirectionToVector2 (direction);
		SetDirection (_dir);
	}

	/// <summary>
	/// Changes the direction on the current playing state.
	/// </summary>
	/// <param name="direction">Direction.</param>
	public void SetDirection (Vector2 direction) {
		currDirection = direction;
		if (currentState != null) {
			currentState.SetStateDirection (currDirection);
		}
		SetCurrentClip (false);
	}


	/// <summary>
	/// Changes the current state by name. 
	/// If the state is the same than the previous one, it will not restart it.
	/// </summary>
	/// <param name="name">Name of the state to play.</param>
	public void SetState (string name) {
		if (canExit) {
			if (!stateDictionnary.ContainsKey (name)) {
				Debug.LogError ("Dictionnary error, the name of the state is not recognized");
			} else {
				int currHash = stateDictionnary [name].GetHashCode ();
				if (currHash != lastStateHash) {
					currentState = stateDictionnary [name];
					OnStateChange ();
					lastStateHash = currHash;
				}
			}
		} else {
			nextToCheck = name;
		}
	}

	/// <summary>
	/// Changes the current state by its index in the list. 
	/// If the state is the same than the previous one, it will not restart it.
	/// </summary>
	/// <param name="name">Index of the state to play in the list.</param>
	public void SetState (int index) {
		SetState (states [index].name);
	}

	/// <summary>
	/// Updates the dictionnary for calling states by name. 
	/// Only call if you update the list at runtime.
	/// </summary>
	public void UpdateDictionnary() {
		stateDictionnary.Clear ();
		foreach (AnimState st in states) {
			if (st != null) {
				if (!stateDictionnary.ContainsKey (st.name)) {
					stateDictionnary.Add (st.name, st);
				}
			}
		}
	}

	// Allow the animation to exit on exit time, if hasExitTime is true.
	void ExitOnTime () {
		if (currentState != null) {
			if (!canExit && currStateTime >= currentState.exitTime) {
				canExit = true;
				if (currentState.onFinish == OnFinishAnim.GoToChosenState) {
					SetState (currentState.nextState.name);
				} else {
					if (nextToCheck != null) {
						SetState (nextToCheck);
						nextToCheck = null;
					}
				}
			}
		}
	}

	// Called when the anim2d state changes.
	void OnStateChange () {
		canExit = !currentState.hasExitTime;
		currentState.SetStateDirection (currDirection);
		SetCurrentClip (true);
	}

	// Called when direction change or when state changes.
	void SetCurrentClip (bool changedState) {
		if (currentState != null) {
			int currHash = currentState.GetCurrentClip ().GetHashCode ();
			if (currHash != lastClipHash) {
				OnClipChange (changedState);
				lastClipHash = currHash;
			}
		}
	}

	// Called when the animator has to change the current clip.
	void OnClipChange (bool changedState) {
		currentClip = currentState.GetCurrentClip ();
		float newStateTime = 0;
		if (!changedState) {
			newStateTime = currStateTime;
		}
		PlayCurrentClip (newStateTime);
	}

	// Play the current clip.
	void PlayCurrentClip (float time) {
		animator.Play (currentClip.animationClip.name, -1, time);
		HandleFlip ();
	}

	// Flips the animation by the chosen method.
	void HandleFlip () {
		if (spriteRenderer != null) {
			spriteRenderer.flipX = (currentClip.flip == AnimFlipStyle.FlipSprite);
		}
		bool currFlipScale = currentClip.flip == AnimFlipStyle.FlipScale;
		if (currFlipScale != lastFlipScale) {
			Vector3 currScale = transform.localScale;
			currScale.x *= -1f;
			transform.localScale = currScale;
			lastFlipScale = currFlipScale;
		}
		bool currFlipAngle = currentClip.flip == AnimFlipStyle.FlipAngle;
		if (currFlipAngle != lastFlipAngle) {
			Vector3 currRotation = transform.localRotation.eulerAngles;
			currRotation.y += 180f;
			transform.localRotation = Quaternion.Euler (currRotation);
			lastFlipAngle = currFlipAngle;
		}
	}

	// Set Automatic direction by the chosen method.
	void AutomaticDirection () {
		if (autoDirection == AutoDirectionType.UseTransformXY || autoDirection == AutoDirectionType.UseTransformXZ) {
			currPosition = transform.position;
			if (currPosition != lastPosition) {
				Vector3 positionDelta = currPosition - lastPosition;
				Vector2 dir = (Vector2) positionDelta;
				if (autoDirection == AutoDirectionType.UseTransformXZ) {
					dir.y = positionDelta.z;
				}
				SetDirection (dir.normalized);
				lastPosition = currPosition;
			}
		} else if (autoDirection == AutoDirectionType.UseInputAxes) {
			Vector2 dir = new Vector2 (Input.GetAxis ("Horizontal"), Input.GetAxis ("Vertical"));
			if (dir.magnitude > 0.003f) {
				SetDirection (dir);
			}
		}
	}

	AnimationClip[] GetAllClips () {
		List<AnimationClip> allClips = new List<AnimationClip> ();
		foreach (AnimState st in states) {
			allClips.AddRange (st.GetAllClips ());
		}
		AnimationClip[] results = allClips.Distinct ().ToArray ();
		return results;
	}

	// Get all needed components.
	void GrabComponents () {
		if (animator == null) {
			animator = GetComponent<Animator> ();
		}
		if (spriteRenderer == null) {
			spriteRenderer = GetComponent<SpriteRenderer> ();
		}
	}
}