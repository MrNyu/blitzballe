﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterTestScript : MonoBehaviour {

	Rigidbody2D rb2d;
	Anim2DPlayer animPlayer;

	// Use this for initialization
	void Start () {
		animPlayer = GetComponent<Anim2DPlayer> ();
		rb2d = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (rb2d.velocity.magnitude < 0.01f) {
			animPlayer.SetState ("Perso-idle-STATE");
		} else {
			animPlayer.SetState ("Perso-walk-STATE");
		}
	}
}
