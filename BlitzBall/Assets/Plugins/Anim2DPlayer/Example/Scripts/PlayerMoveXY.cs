﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMoveXY : MonoBehaviour {
	
	[Header("Inputs")]
	public string horizontalAxis = "Horizontal";
	public string verticalAxis = "Vertical";

	Rigidbody2D rb2d;

	[Header("Déplacements")]
	public float speed = 5f;
	public float acceleration = 1f;
	public float deceleration = 1f;

	Vector2 refVelocity;
	Vector2 inputDirection;
	Vector2 dampedDirection;
	Vector2 finalVelocity;

	void Awake () {
		rb2d = GetComponent<Rigidbody2D> ();
	}

	// Update is called once per frame
	void Update () {
		inputDirection.x = Input.GetAxis (horizontalAxis);
		inputDirection.y = Input.GetAxis (verticalAxis);
		inputDirection = Vector2.ClampMagnitude (inputDirection, 1f);

		if (inputDirection.magnitude < dampedDirection.magnitude) {
			dampedDirection = Vector2.SmoothDamp (dampedDirection, inputDirection, ref refVelocity, (1f / (deceleration * 50f)), 1000f, Time.deltaTime);
		} else {
			dampedDirection = Vector2.SmoothDamp (dampedDirection, inputDirection, ref refVelocity, (1f / (acceleration * 50f)), 1000f, Time.deltaTime);
		}

		finalVelocity = (dampedDirection * speed * 100f * Time.deltaTime);

		rb2d.velocity = finalVelocity;
	}
}
