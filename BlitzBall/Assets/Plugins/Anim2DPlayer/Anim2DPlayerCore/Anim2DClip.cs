﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Anim2D {
	public enum AnimFlipStyle { NoFlip, FlipSprite, FlipScale, FlipAngle }

	[System.Serializable]
	public class AnimClip {
		public AnimationClip animationClip;
		public AnimFlipStyle flip = AnimFlipStyle.NoFlip;
	}
}