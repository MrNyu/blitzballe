﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Build;
using UnityEditorInternal;

[CustomEditor(typeof(Anim2DPlayer))]
public class Anim2DPlayerEditor : Editor {

	public Anim2DPlayer obj;
	private ReorderableList list;
	Rect currRect;
	Texture2D logo;
	SerializedProperty autoDirection;
	SerializedProperty playOnAwake;

	private void OnEnable() {
		SetListCallbacks ();
		string logoPath = EditorGUIUtility.isProSkin ? "anim2dplayer-editor-icon-dark" : "anim2dplayer-editor-icon";
		logo = (Texture2D) Resources.Load (logoPath, typeof(Texture2D));
	}

	public override void OnInspectorGUI () {
		SetProperties ();
		GUILayout.Space (5f);
		serializedObject.Update();
		EditorGUI.BeginChangeCheck ();

		DoInspectorGUI ();

		if (EditorGUI.EndChangeCheck ()) {
			obj.UpdateDictionnary();
		}
		serializedObject.ApplyModifiedProperties();
		GUILayout.Space (12f);
	}

	void SetProperties () {
		obj = (Anim2DPlayer) target;
		autoDirection = serializedObject.FindProperty ("autoDirection");
		playOnAwake = serializedObject.FindProperty ("playFirstOnAwake");
	}

	void DoInspectorGUI () {
		GUILayout.Space (3);
		GUILayout.Label (logo);
		GUILayout.Space (8);
		EditorGUILayout.LabelField ("Direction", EditorStyles.boldLabel);
		EditorGUILayout.PropertyField (autoDirection);
		GUILayout.Space (8);
		EditorGUILayout.LabelField ("Animation States", EditorStyles.boldLabel);
		list.DoLayoutList();

		EditorGUILayout.LabelField ("Options", EditorStyles.boldLabel);
		EditorGUILayout.PropertyField (playOnAwake);
	}

	void SetListCallbacks () {
		list = new ReorderableList(
			serializedObject, 
			serializedObject.FindProperty("states"), 
			true, true, true, true
		);

		// Dropdown add button
		list.onAddDropdownCallback = (Rect buttonRect, ReorderableList l) => {
			obj.states.Add(null);
		};

		// Draw elements
		list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
			SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);
			currRect = rect;
			currRect.height = EditorGUIUtility.singleLineHeight;
			currRect.x += 5;
			currRect.width -= 10;

			EditorGUI.PropertyField(currRect, element, GUIContent.none, false);

			rect.y += 2;
		};

		// Header
		list.drawHeaderCallback = (Rect rect) => {  
			EditorGUI.LabelField(rect, "States");
		};
	}

}
