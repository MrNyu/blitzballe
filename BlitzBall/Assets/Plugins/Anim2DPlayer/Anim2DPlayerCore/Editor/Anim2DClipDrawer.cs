﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(Anim2D.AnimClip))]
public class Anim2DStateSideDrawer : PropertyDrawer {

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
		label = EditorGUI.BeginProperty(position, label, property);
		position = EditorGUI.PrefixLabel(position, label);

		SerializedProperty clip = property.FindPropertyRelative ("animationClip");
		SerializedProperty flip = property.FindPropertyRelative ("flip");


		Rect clipPosition = position;
		Rect flipPosition = position;
		float currHorizontal;

		clipPosition.width = position.width * 0.6f;
		currHorizontal = clipPosition.x + clipPosition.width + 10;
		flipPosition.x = currHorizontal;
		flipPosition.width = position.width * 0.4f - 10;


		EditorGUI.BeginChangeCheck();
		int indent = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;

		EditorGUI.PropertyField (clipPosition, clip, GUIContent.none, false);
		EditorGUI.PropertyField (flipPosition, flip, GUIContent.none, false);


		if (EditorGUI.EndChangeCheck ()) {
			property.serializedObject.ApplyModifiedProperties ();
		}
		EditorGUI.indentLevel = indent;
		EditorGUI.EndProperty();
	}
}
