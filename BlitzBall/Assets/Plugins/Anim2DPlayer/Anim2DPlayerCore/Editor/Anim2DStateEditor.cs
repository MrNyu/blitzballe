﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Anim2D;

[CustomEditor(typeof(AnimState), true)]
public class Anim2DStateEditor : Editor {

	Texture2D logo;
	AnimState obj;
	SerializedProperty hasExitTime;
	SerializedProperty exitTime;
	SerializedProperty onFinish;
	SerializedProperty nextState;

	public override void OnInspectorGUI () {
		SetProperties ();
		GUILayout.Space (5f);
		serializedObject.Update ();
		MainInspector ();
		serializedObject.ApplyModifiedProperties ();
		GUILayout.Space (5f);
	}

	void MainInspector () {
		GUILayout.Label (logo);
		GUILayout.Space (8f);
		EditorGUILayout.LabelField ("Directions Animation Clips", EditorStyles.boldLabel);
		DrawClipList ();
		GUILayout.Space (8f);
		EditorGUILayout.LabelField ("Options", EditorStyles.boldLabel);
		EditorGUILayout.PropertyField (hasExitTime);
		if (obj.hasExitTime) {
			EditorGUILayout.PropertyField (exitTime);
			EditorGUILayout.PropertyField (onFinish);
			if (obj.onFinish == OnFinishAnim.GoToChosenState) {
				EditorGUILayout.PropertyField (nextState);
			}
		}
	}

	void SetProperties () {
		obj = (AnimState) target;
		hasExitTime = serializedObject.FindProperty ("hasExitTime");
		exitTime = serializedObject.FindProperty ("exitTime");
		onFinish = serializedObject.FindProperty ("onFinish");
		nextState = serializedObject.FindProperty ("nextState");
		LoadLogo ();
	}

	void DrawClipList () {
		if (obj is AnimStateHorizontal) {
			SerializedProperty left = serializedObject.FindProperty ("left");
			SerializedProperty right = serializedObject.FindProperty ("right");
			EditorGUILayout.PropertyField (left);
			EditorGUILayout.PropertyField (right);
		} else if (obj is AnimStateVertical) {
			SerializedProperty up = serializedObject.FindProperty ("up");
			SerializedProperty down = serializedObject.FindProperty ("down");
			EditorGUILayout.PropertyField (up);
			EditorGUILayout.PropertyField (down);
		} else if (obj is AnimStateFourSides) {
			SerializedProperty up = serializedObject.FindProperty ("up");
			SerializedProperty down = serializedObject.FindProperty ("down");
			SerializedProperty left = serializedObject.FindProperty ("left");
			SerializedProperty right = serializedObject.FindProperty ("right");
			EditorGUILayout.PropertyField (left);
			EditorGUILayout.PropertyField (right);
			EditorGUILayout.PropertyField (up);
			EditorGUILayout.PropertyField (down);
		} else if (obj is AnimStateEightSides) {
			SerializedProperty up = serializedObject.FindProperty ("up");
			SerializedProperty down = serializedObject.FindProperty ("down");
			SerializedProperty left = serializedObject.FindProperty ("left");
			SerializedProperty right = serializedObject.FindProperty ("right");
			SerializedProperty upLeft = serializedObject.FindProperty ("upLeft");
			SerializedProperty upRight = serializedObject.FindProperty ("upRight");
			SerializedProperty downLeft = serializedObject.FindProperty ("downLeft");
			SerializedProperty downRight = serializedObject.FindProperty ("downRight");
			EditorGUILayout.PropertyField (up);
			EditorGUILayout.PropertyField (upRight);
			EditorGUILayout.PropertyField (right);
			EditorGUILayout.PropertyField (downRight);
			EditorGUILayout.PropertyField (down);
			EditorGUILayout.PropertyField (downLeft);
			EditorGUILayout.PropertyField (left);
			EditorGUILayout.PropertyField (upLeft);
		} else if (obj is AnimStateSingle) {
			SerializedProperty clip = serializedObject.FindProperty ("clip");
			EditorGUILayout.PropertyField (clip);
		}
	}

	string LogoPath () {
		string logoPath = "anim2dstate-";
		if (obj is AnimStateHorizontal) {
			logoPath += "horizontal";
		} else if (obj is AnimStateVertical) {
			logoPath += "vertical";
		} else if (obj is AnimStateFourSides) {
			logoPath += "foursides";
		} else if (obj is AnimStateEightSides) {
			logoPath += "eightsides";
		} else if (obj is AnimStateSingle) {
			logoPath += "single";
		}
		logoPath += "-title";
		if (EditorGUIUtility.isProSkin) {
			logoPath += "-dark";
		}
		return logoPath;
	}

	void LoadLogo () {
		logo = (Texture2D) Resources.Load (LogoPath(), typeof(Texture2D));
	}
}
