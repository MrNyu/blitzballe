﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Anim2D {
	public class ClipAngleZone {
		public AnimClip animClip;
		float baseAngle;
		float angleSize;
		public float minExtreme;
		public float maxExtreme;

		public ClipAngleZone (AnimClip _animClip, float _baseAngle, float _angleSize) {
			animClip = _animClip;
			baseAngle = _baseAngle;
			angleSize = _angleSize;
			CalculateExtremes ();
		}

		public void CalculateExtremes () {
			minExtreme = baseAngle - (angleSize * 0.5f);
			maxExtreme = baseAngle + (angleSize * 0.5f);
		}

		public bool CheckIfOverlap (ClipAngleZone other) {
			bool overlap = maxExtreme >= other.minExtreme && other.maxExtreme >= minExtreme;
			return overlap;
		}
	}

	public static class AnimAngles {
		public static float DegToNorm (float degrees) {
			return (degrees / 360f);
		}

		public static float RadToNorm (float radians) {
			return (radians / (Mathf.PI * 2));
		}

		public static float NormToDeg (float normalizedAngle) {
			return (normalizedAngle * 360f);
		}

		public static float NormToRad (float normalizedAngle) {
			return (normalizedAngle * Mathf.PI * 2);
		}
	}
}