﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Anim2D {
	public enum AnimDirection { Down, Up, Left, Right, UpLeft, UpRight, DownLeft, DownRight };
	public enum OnFinishAnim { FindNextState, GoToChosenState };

	// Classe de base
	public class AnimState : ScriptableObject {
		protected Vector2 currDirection;

		public bool hasExitTime = false;
		public float exitTime = 1f;
		public OnFinishAnim onFinish = OnFinishAnim.FindNextState;
		public AnimState nextState;

		public virtual void SetStateDirection (Vector2 direction) {
			currDirection = direction.normalized;
		}

		public virtual AnimClip GetCurrentClip () {
			return new AnimClip ();
		}

		public virtual AnimationClip[] GetAllClips () {
			return new AnimationClip[0];
		}

		public Vector2 GetCurrentDirection () {
			return currDirection;
		}

		public static Vector2 AnimDirectionToVector2 (AnimDirection _dir) {
			switch (_dir) {
			case AnimDirection.DownLeft:
				return new Vector2 (-1, -1).normalized;
			case AnimDirection.DownRight:
				return new Vector2 (1, -1).normalized;
			case AnimDirection.UpLeft:
				return new Vector2 (-1, 1).normalized;
			case AnimDirection.UpRight:
				return new Vector2 (1, 1).normalized;
			case AnimDirection.Down:
				return Vector2.down;
			case AnimDirection.Up:
				return Vector2.up;
			case AnimDirection.Left:
				return Vector2.left;
			default:
				return Vector2.right;
			}
		}
	}
}
