﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Anim2D {
	// Classe pour les anim verticales (haut ou bas)
	[CreateAssetMenu(fileName = "AnimState_Vertical", menuName = "Animation 2D/2 Sides : Vertical")]
	public class AnimStateVertical : AnimState {
		Vector2 lastVerticalDirection = new Vector2 (0, -1);
		public AnimClip down;
		public AnimClip up;
		public float deadZone = 0.01f;

		public override AnimClip GetCurrentClip () {
			if (currDirection.y < -deadZone || currDirection.y > deadZone) {
				lastVerticalDirection = currDirection;
			}
			return lastVerticalDirection.y < 0 ? down : up;
		}

		public override AnimationClip[] GetAllClips () {
			AnimationClip[] clipList = new AnimationClip[2];
			clipList [0] = up.animationClip;
			clipList [1] = down.animationClip;
			return clipList;
		}
	}
}
