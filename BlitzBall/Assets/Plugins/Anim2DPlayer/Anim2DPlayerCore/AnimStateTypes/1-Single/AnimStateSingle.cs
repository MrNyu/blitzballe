﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Anim2D {
	// Classe pour les anim seules
	[CreateAssetMenu(fileName = "AnimState_Single", menuName = "Animation 2D/Single")]
	public class AnimStateSingle : AnimState {
		public AnimClip clip;

		public override AnimClip GetCurrentClip () {
			return clip;
		}

		public override AnimationClip[] GetAllClips () {
			AnimationClip[] clipList = new AnimationClip[1];
			clipList [0] = clip.animationClip;
			return clipList;
		}
	}
}
