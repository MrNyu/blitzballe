﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Anim2D {
	// Classe pour les anim horizontales (droite ou gauche)
	[CreateAssetMenu(fileName = "AnimState_Horizontal", menuName = "Animation 2D/2 Sides : Horizontal")]
	public class AnimStateHorizontal : AnimState {
		Vector2 lastHorizontalDirection = new Vector2 (1, 0);
		public AnimClip left;
		public AnimClip right;
		public float deadZone = 0.01f;

		public override AnimClip GetCurrentClip () {
			if (currDirection.x < -deadZone || currDirection.x > deadZone) {
				lastHorizontalDirection = currDirection;
			}
			return lastHorizontalDirection.x < 0 ? left : right;
		}

		public override AnimationClip[] GetAllClips () {
			AnimationClip[] clipList = new AnimationClip[2];
			clipList [0] = left.animationClip;
			clipList [1] = right.animationClip;
			return clipList;
		}
	}
}
