﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Anim2D {
	// Classes pour les anims à 4 directions (haut, bas, gauche, droite)
	[CreateAssetMenu(fileName = "AnimState_FourSides", menuName = "Animation 2D/4 Sides")]
	public class AnimStateFourSides : AnimState {
		public AnimClip up;
		public AnimClip down;
		public AnimClip left;
		public AnimClip right;
		AnimClip lastClip;
		bool lastMoveIsVertical;
		const float deadZone = 0.1f;

		public override AnimClip GetCurrentClip () {
			float _deadZone = lastMoveIsVertical ? -deadZone : deadZone;
			bool verticalMove = Mathf.Abs (currDirection.y) > (Mathf.Abs (currDirection.x) + _deadZone);
			lastMoveIsVertical = verticalMove;
			if (verticalMove) {
				return currDirection.y < 0 ? down : up;
			} else {
				return currDirection.x < 0 ? left : right;
			}
		}

		public override AnimationClip[] GetAllClips () {
			AnimationClip[] clipList = new AnimationClip[4];
			clipList [0] = up.animationClip;
			clipList [1] = down.animationClip;
			clipList [2] = left.animationClip;
			clipList [3] = right.animationClip;
			return clipList;
		}
	}
}
