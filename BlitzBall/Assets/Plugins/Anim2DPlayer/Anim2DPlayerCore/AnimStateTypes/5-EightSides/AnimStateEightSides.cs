﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Anim2D {
	// Classes pour les anims à 4 directions (haut, bas, gauche, droite)
	[CreateAssetMenu(fileName = "AnimState_EightSides", menuName = "Animation 2D/8 Sides (with diagonals)")]
	public class AnimStateEightSides : AnimState {
		public AnimClip up;
		public AnimClip upRight;
		public AnimClip right;
		public AnimClip downRight;
		public AnimClip down;
		public AnimClip downLeft;
		public AnimClip left;
		public AnimClip upLeft;
		const float baseUnit = Mathf.PI / 8f;

		public override AnimClip GetCurrentClip () {
			float radianAngle = Vector2ToRad (currDirection);
			if (radianAngle.FloatIsBetween (baseUnit, baseUnit * 9)) {
				if (radianAngle.FloatIsBetween (baseUnit, baseUnit * 3)) {
					return upRight;
				} else if (radianAngle.FloatIsBetween (baseUnit * 3, baseUnit * 5)) {
					return up;
				} else if (radianAngle.FloatIsBetween (baseUnit * 5, baseUnit * 7)) {
					return upLeft;
				} else {
					return left;
				}
			} else {
				if (radianAngle.FloatIsBetween (baseUnit * 9, baseUnit * 11)) {
					return downLeft;
				} else if (radianAngle.FloatIsBetween (baseUnit * 11, baseUnit * 13)) {
					return down;
				} else if (radianAngle.FloatIsBetween (baseUnit * 13, baseUnit * 15)) {
					return downRight;
				} else {
					return right;
				}
			}
		}
		
		public float Vector2ToRad (Vector2 vector) {
			float angle = Vector2.Angle (Vector2.right, vector);
			angle = vector.y < 0 ? (360 - angle) : angle;
			return angle * Mathf.Deg2Rad;
		}

		public override AnimationClip[] GetAllClips () {
			AnimationClip[] clipList = new AnimationClip[8];
			clipList [0] = up.animationClip;
			clipList [1] = down.animationClip;
			clipList [2] = left.animationClip;
			clipList [3] = right.animationClip;
			clipList [4] = upLeft.animationClip;
			clipList [5] = upRight.animationClip;
			clipList [6] = downLeft.animationClip;
			clipList [7] = downRight.animationClip;
			return clipList;
		}
	}

	public static class AnimStateExt {
		public static bool FloatIsBetween (this float nb, float min, float max) {
			return (nb > min && nb < max);
		}
	}
}
