﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostFx : MonoBehaviour
{
    public PostProcessVolume fx;
    public int fxState = 0;
    float duration;
    public int maxState;

    void Start()
    {
        fxState = 0;
    }
    void Update()
    {   if(fxState>=maxState)
        fxState = maxState;
        if(fxState == 0)
        {
            fx.weight = 0;
        }
        else
        {
            fx.weight = Mathf.Lerp(fx.weight, (1f/maxState)*fxState, duration*Time.unscaledDeltaTime);
        }
    }
    public void BOOMMUDAFUKAZ(float time) {
        if(fxState>=maxState)
            return;
        duration = time;
        fxState++;
    }
    
    public void ResetFXState() {
        fxState = 0;
    }
}
