﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class But : MonoBehaviour
{
    int bank = 0;
    public int Nstock;
    public GameObject player;
    public GameObject player2;
    public GameObject ScoreDisplay;
    public GameObject sprite;
    public GameObject Anim;

    public enum Team
    {
        Blue,
        Red
    }
    public Team team;
    public enum ButState
    {
        State1,
        State2,
        State3,
        State4,
        State5,
        State6
    }
    public ButState butState;
    public Bitcoin RefBitcoin; 
	public GameObject Score;
	public int LastBitcoin;

    public GameObject but;
    public GameObject but2;
    public GameObject but3;
    public GameObject mur;
    public GameObject mur2;
    public GameObject mur3;
    
    public GameObject lum;
    public GameObject lum2;
    public GameObject lum3;
    public GameObject lum4;
    public GameObject luminté;
    public GameObject luminté2;

    public GameObject potal;
    public GameObject potal2;
    public GameObject potal3;
    public GameObject potal4;
    public GameObject potalinté;
    public GameObject potalinté1;

    public Sprite lumon;
    public Sprite lumoff;

    public Sprite potalon;
    public Sprite potal2on;
    public Sprite potal3on;
    public Sprite potal4on;
    public Sprite potalintéon;
    public Sprite potalinté1on;

    public Sprite potalintéoff;
    public Sprite potalmidoff;
    public Sprite potaltopoff;
    public Sprite potalbotoff;

    public BoxCollider2D hitbox;
    public BoxCollider2D hitbox2;
    public BoxCollider2D hitbox3;

    GameObject Parameter;
    public float Typegoal;

    // add soundesign de but;
    public AudioSource SFX;
    public AudioClip buuut;

    public void Start(){
       SFX = GameObject.Find("/AudioSource/SFX").GetComponent<AudioSource>(); 
    }

    public void Update(){
        Parameter = GameObject.Find("/ParameterManager");

        Typegoal = Parameter.GetComponent<ParameterManager>().goall;

        if (Typegoal == 1){
            hitbox2.enabled = false;
            hitbox3.enabled = false;
            hitbox.enabled = true;
        }
        if (Typegoal == 2){
            hitbox.enabled = false;
            hitbox2.enabled = true;
            hitbox3.enabled = true;
        }
        ButSizeUpdate();
    }
     
    public void ButSizeUpdate()
    {
        Vector2 hight1 = new Vector2(0.3747983f,3.921579f);
        Vector2 hight2 = new Vector2(0.3747983f,5.839456f);
        Vector2 hight3 = new Vector2(0.3747983f,7.65385f);

        Vector2 hight4 = new Vector2(0.3747983f,0.9135127f);
        Vector2 hight5 = new Vector2(0.3747983f,1.828382f);
        Vector2 hight6 = new Vector2(0.3747983f,4.123973f);
        Vector2 offset4 = new Vector2(0,3.36595f);
        Vector2 offset5 = new Vector2(0,2.908515f);
        Vector2 offset6 = new Vector2(0,1.76072f);

        if(Nstock == 0 && Typegoal == 1) butState = ButState.State1;
        if(Nstock == 1 && Typegoal == 1) butState = ButState.State2;
        if(Nstock == 2 && Typegoal == 1) butState = ButState.State3;
        if(Nstock == 0 && Typegoal == 2) butState = ButState.State4;
        if(Nstock == 1 && Typegoal == 2) butState = ButState.State5;
        if(Nstock == 2 && Typegoal == 2) butState = ButState.State6;

        //visuel changeant pour chaque but, change sprite du but, des marques au murs et ceux des bar et lumières
        switch(butState)
        {
            case ButState.State1:
                hitbox.size = hight1;
                luminté.GetComponent<SpriteRenderer>().sprite = lumon;
                luminté2.GetComponent<SpriteRenderer>().sprite = lumon;
                lum.GetComponent<SpriteRenderer>().sprite = lumoff;
                lum2.GetComponent<SpriteRenderer>().sprite = lumoff;
                lum3.GetComponent<SpriteRenderer>().sprite = lumoff;
                lum4.GetComponent<SpriteRenderer>().sprite = lumoff;
                potalinté.GetComponent<SpriteRenderer>().sprite = potalintéon;
                potalinté1.GetComponent<SpriteRenderer>().sprite = potalintéon;

                but.SetActive(true);
                but2.SetActive(false);
                but3.SetActive(false);
                mur.SetActive(true);
                mur2.SetActive(false);
                mur3.SetActive(false);

            break;

            case ButState.State2:
              hitbox.size = hight2;
              but2.SetActive(true);
              mur2.SetActive(true);

                lum.GetComponent<SpriteRenderer>().sprite = lumon;
                lum2.GetComponent<SpriteRenderer>().sprite = lumon;
                luminté.GetComponent<SpriteRenderer>().sprite = lumoff;
                luminté2.GetComponent<SpriteRenderer>().sprite = lumoff;
                potal.GetComponent<SpriteRenderer>().sprite = potalon;
                potal2.GetComponent<SpriteRenderer>().sprite = potal2on;
                potalinté.GetComponent<SpriteRenderer>().sprite = potalintéoff;
                potalinté1.GetComponent<SpriteRenderer>().sprite = potalintéoff;

            break;

            case ButState.State3:
              hitbox.size = hight3;
              but3.SetActive(true);
              mur3.SetActive(true);

                lum3.GetComponent<SpriteRenderer>().sprite = lumon;
                lum4.GetComponent<SpriteRenderer>().sprite = lumon;
                lum.GetComponent<SpriteRenderer>().sprite = lumoff;
                lum2.GetComponent<SpriteRenderer>().sprite = lumoff;
                
                luminté.GetComponent<SpriteRenderer>().sprite = lumoff;
                luminté2.GetComponent<SpriteRenderer>().sprite = lumoff;
                potalinté.GetComponent<SpriteRenderer>().sprite = potalintéoff;
                potalinté1.GetComponent<SpriteRenderer>().sprite = potalintéoff;
                potal3.GetComponent<SpriteRenderer>().sprite = potal3on;
                potal4.GetComponent<SpriteRenderer>().sprite = potal4on;
                potal.GetComponent<SpriteRenderer>().sprite = potalmidoff;
                potal2.GetComponent<SpriteRenderer>().sprite = potalmidoff;
            break;


            case ButState.State4:

                hitbox2.size = hight4;
                hitbox3.size = hight4;
                hitbox2.offset = offset4;
                hitbox3.offset = offset4;
                
                but.SetActive(false);
                but2.SetActive(false);
                but3.SetActive(true);
                mur.SetActive(false);
                mur2.SetActive(false);
                mur3.SetActive(true);
                
                lum3.GetComponent<SpriteRenderer>().sprite = lumon;
                lum4.GetComponent<SpriteRenderer>().sprite = lumon;
                lum.GetComponent<SpriteRenderer>().sprite = lumon;
                lum2.GetComponent<SpriteRenderer>().sprite = lumon;
                potal3.GetComponent<SpriteRenderer>().sprite = potal3on;
                potal4.GetComponent<SpriteRenderer>().sprite = potal4on;
                potal.GetComponent<SpriteRenderer>().sprite = potalon;
                potal2.GetComponent<SpriteRenderer>().sprite = potal2on;
                

            break;

            case ButState.State5:
                hitbox2.size = hight5;
                hitbox3.size = hight5;
                hitbox2.offset = offset5;
                hitbox3.offset = offset5;
                // activer les sprite mid et min;
                luminté.GetComponent<SpriteRenderer>().sprite = lumon;
                luminté2.GetComponent<SpriteRenderer>().sprite = lumon;
                potal.GetComponent<SpriteRenderer>().sprite = potalmidoff;
                potal2.GetComponent<SpriteRenderer>().sprite = potalmidoff;
                potalinté.GetComponent<SpriteRenderer>().sprite = potalintéon;
                potalinté1.GetComponent<SpriteRenderer>().sprite = potalintéon;

                but2.SetActive(true);
                mur2.SetActive(true);

            break;

            case ButState.State6:

                but.SetActive(true);
                mur.SetActive(true);

                hitbox2.size = hight6;
                hitbox3.size = hight6;
                hitbox2.offset = offset6;
                hitbox3.offset = offset6;
                // activer les sprite min;
                potalinté.GetComponent<SpriteRenderer>().sprite = potalintéoff;
                potalinté1.GetComponent<SpriteRenderer>().sprite = potalintéoff;

            break;
        }
    }

    public void AddBitcoinToBank()
    {
		LastBitcoin = RefBitcoin.gameObject.GetComponent<Bitcoin>().Bitvalue;
        bank += RefBitcoin.gameObject.GetComponent<Bitcoin>().Bitvalue;
		GameObject newScore = Instantiate(Score,(new Vector3 (player.transform.position.x,player.transform.position.y+1.5f,player.transform.position.z)),new Quaternion(0,0,0,1));
		newScore.GetComponent<Score>().SetText(LastBitcoin);
        if (player.name == "Player"){
            ScoreDisplay.gameObject.GetComponent<ScoreDisplay>().UpScoreP1();
        }
        if (player.name == "Player2"){
            ScoreDisplay.gameObject.GetComponent<ScoreDisplay>().UpScoreP2();
        }
        RefBitcoin.gameObject.GetComponent<Bitcoin>().Bitvalue = RefBitcoin.gameObject.GetComponent<Bitcoin>().Startvalue;
    }

        public void AddBitcoinToBankAdversary()
    {
		LastBitcoin = RefBitcoin.gameObject.GetComponent<Bitcoin>().Bitvalue;
        bank += RefBitcoin.gameObject.GetComponent<Bitcoin>().Bitvalue;

		GameObject newScore = Instantiate(Score,(new Vector3 (player2.transform.position.x,player.transform.position.y+1.5f,player.transform.position.z)),new Quaternion(0,0,0,1));
		newScore.GetComponent<Score>().SetText(LastBitcoin);
        if (player.name == "Player"){
            ScoreDisplay.gameObject.GetComponent<ScoreDisplay>().UpScoreP2();
        }
        if (player.name == "Player2"){
            ScoreDisplay.gameObject.GetComponent<ScoreDisplay>().UpScoreP1();
        }
        RefBitcoin.gameObject.GetComponent<Bitcoin>().Bitvalue = RefBitcoin.gameObject.GetComponent<Bitcoin>().Startvalue;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if( other.gameObject.CompareTag("Bitcoin"))
        {
            switch(team)
            {
                case Team.Blue:
                    //goalAnim.SetBool("blue/red",false);
                break;

                case Team.Red:
                    //goalAnim.SetBool("blue/red",true);
                break;
            }
            //goalAnim.SetTrigger("Goal");
            AddBitcoinToBankAdversary();
            other.GetComponent<Bitcoin>().Spawn();
            //lancer l'anim wavebut;
            Anim.SetActive(true);
            SFX.clip = buuut;
            SFX.Play();
            Invoke("stopanim", 0.7f);
        }

        if( other.gameObject.CompareTag("Fastcoin"))
        {
            switch(team)
            {
                case Team.Blue:
                    //goalAnim.SetBool("blue/red",false);
                break;
                case Team.Red:
                    //goalAnim.SetBool("blue/red",true);
                break;
            }
            //goalAnim.SetTrigger("Goal"); // anim but
            AddBitcoinToBankAdversary();
            other.GetComponentInParent<Bitcoin>().Spawn();
            //lancer l'anim wavebut;
            Anim.SetActive(true);
            SFX.clip = buuut;
            SFX.Play();
            Invoke("stopanim", 0.7f);
        }
    }
    void stopanim(){
        Anim.SetActive(false);
    }
}