﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameRules : MonoBehaviour
{
    public GameObject Parameter;
    float end;
    bool bump;
    public GameObject bumpers;
    public GameObject ScoreMaxi;

    public void Start(){

        Parameter = GameObject.Find("/ParameterManager");
        end = Parameter.GetComponent<ParameterManager>().end;
        bump = Parameter.GetComponent<ParameterManager>().Bumper;
        
        if (bump == false){
        bumpers.SetActive(false);
        }
        if(bump == true){
        bumpers.SetActive(true);
        }
        if (end ==1){
            // active la fin par timer et le timer et désactive le script de fin par point
            GameObject.Find("/Canvas/Timer").SetActive(true);
            GameObject.Find("/Score&TimerManager").GetComponent<TimerEnd>().enabled = true;
            GameObject.Find("/Score&TimerManager").GetComponent<TimerEnd>().timeend = true;
            GameObject.Find("/Score&TimerManager").GetComponent<ScoreDisplay>().point = false;
            ScoreMaxi.SetActive(false);
        }
        if (end ==2){
            // désactive fin par timer et le timer et active le script de fin par point
            GameObject.Find("/Canvas/Timer").SetActive(false);
            GameObject.Find("/Score&TimerManager").GetComponent<TimerEnd>().enabled = false;
            GameObject.Find("/Score&TimerManager").GetComponent<TimerEnd>().timeend = false;
            GameObject.Find("/Score&TimerManager").GetComponent<ScoreDisplay>().point = true;
        }
    }
}