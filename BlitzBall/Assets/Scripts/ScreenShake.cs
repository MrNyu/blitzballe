﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Core.Easing;
using DG.Tweening.Core.Enums;

public class ScreenShake : MonoBehaviour
{
    public Camera mainCamera;

    public void shake( float duration = 0.4f, float strenght = 0.1f, int vibrato = 10, float randomness = 90) {
        mainCamera.DOShakePosition (duration, strenght, vibrato, randomness);
    }
    public void Update(){
    this.transform.position = new Vector3 (0,0,-10);
    }
}