﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TimerEnd : MonoBehaviour {

	float timer;
	public bool timeend;
	public Text time;
	public GameObject Parameter;
    public float ScoreP1;
    public float ScoreP2;
	public GameObject imgend;
	public Text txtend;


	IEnumerator Scenechanges() {
    yield return new WaitForSeconds (1f);
    SceneManager.LoadScene(0);
	}

	public void Start(){
		imgend.SetActive(false);
		Parameter = GameObject.Find("/ParameterManager");
		timer = Parameter.GetComponent<ParameterManager>().timer;
	}
	
	void Update(){
		if(timer < 0){
			timer = 0;
		}
		ScoreP1 = GetComponent<ScoreDisplay>().ScoreP1;
		ScoreP2 = GetComponent<ScoreDisplay>().ScoreP2;
		if (timeend == true){
		time.text = timer.ToString("0");
		timer -= Time.deltaTime;
		if(timer<=0)
		{
			StartCoroutine(Scenechanges());
		    if(ScoreP1 > ScoreP2){
			imgend.SetActive(true);
			txtend.text = "Joueur 1 Win";
            Debug.Log(" Joueur 1 (RedSide) Gagne");
        }
        	if(ScoreP2 > ScoreP1){
			imgend.SetActive(true);
			txtend.text = "Joueur 2 Win";
            Debug.Log(" Joueur 2 (BlueSide) Gagne");  
        }
			if(ScoreP2 == ScoreP1){
			imgend.SetActive(true);
			txtend.text = "Egalité";
            Debug.Log(" égalité !"); 
        }
		}
		}
	}
}