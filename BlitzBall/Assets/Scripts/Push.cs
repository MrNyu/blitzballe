﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Push : MonoBehaviour
{
    public GameObject player;
    public Rigidbody2D rb;
    Vector2 pushed;
    public bool push;
    
    public void Awake(){
        push = false;
        pushed = rb.velocity.normalized;
    }

	public void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.CompareTag("Fastcoin"))
        {
            push = true;
            pushed = rb.velocity.normalized*2;
            if( pushed.x <= 0){
            player.transform.position += new Vector3 (pushed.x, pushed.y, 0);
            //other.GetComponentInParent<Bitcoin>().Double(2,Vector2.right);
            }
            push = false;
		}
	}
}