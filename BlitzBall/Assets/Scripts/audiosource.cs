﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audiosource : MonoBehaviour{
    
    private static audiosource instanceRef;
    
    public void Awake() {
        if(instanceRef == null){
            instanceRef = this;
            DontDestroyOnLoad(gameObject);
        }else {
            DestroyImmediate(gameObject);
            }
    }
    public void Start(){
        DontDestroyOnLoad(this.gameObject);
    }
}