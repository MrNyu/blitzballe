﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerSelection : MonoBehaviour
{
    public GameObject manager;
    
    public Sprite Shero;
    public Sprite Shero2;
    public Sprite Shero3;
    public Sprite Shero4;
    public Sprite Shero5;
    public Sprite Shero6;
    public Image selectedhero;
    public Image selectedhero2;

    public void Awake(){
       manager = GameObject.Find("/ParameterManager");
    }

    public void End1(){
        manager.GetComponent<ParameterManager>().End1();
    }
    public void End2(){
        manager.GetComponent<ParameterManager>().End2();
    }
    public void bumper(){
        manager.GetComponent<ParameterManager>().bumper();
    }
    public void nobumper(){
        manager.GetComponent<ParameterManager>().nobumper();
    }
    public void Goal1(){
        manager.GetComponent<ParameterManager>().Goal();
    }
    public void Goal2(){
        manager.GetComponent<ParameterManager>().Goal2();
    }
    public void Go(){
        SceneManager.LoadScene(2);
    }

    public void point1(){
        manager.GetComponent<ParameterManager>().points1();
    }
    public void point2(){
        manager.GetComponent<ParameterManager>().points2();
    }
    public void point3(){
        manager.GetComponent<ParameterManager>().points3();
    }
    public void point4(){
        manager.GetComponent<ParameterManager>().points4();
    }
    public void timer1(){
        manager.GetComponent<ParameterManager>().time1();
    }
    public void timer2(){
        manager.GetComponent<ParameterManager>().time2();
    }
    public void timer3(){
        manager.GetComponent<ParameterManager>().time3();
    }
    public void timer4(){
        manager.GetComponent<ParameterManager>().time4();
    }
    public void J1Skin1(){
        manager.GetComponent<ParameterManager>().heroJ1blu();
        selectedhero.sprite = Shero2;
    }
    public void J1Skin2(){
        manager.GetComponent<ParameterManager>().heroJ1red();
        selectedhero.sprite = Shero4;
    }
    public void J2Skin1(){
        manager.GetComponent<ParameterManager>().heroJ2blu();
        selectedhero2.sprite = Shero;
    }
    public void J2Skin2(){
        manager.GetComponent<ParameterManager>().heroJ2red();
        selectedhero2.sprite = Shero3;
    }   
    public void J1Skin3(){
        manager.GetComponent<ParameterManager>().hero3blu();
        selectedhero.sprite = Shero6;
    }
    public void J2Skin3(){
        manager.GetComponent<ParameterManager>().hero3red();
        selectedhero2.sprite = Shero5;
    }
}