﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
using ActionCode2D.Renderers;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
    
    private Player player;
    public float Speed;
    public int playerId = 0;
    private bool Hit;
    bool Stock;
    public bool attaking = false;
    public float atkTimer;
    float dashTimer;
    Vector3 posOnStartDash;
	Vector3 posEndDash;
    public Vector2 move;
    bool Dash;
    bool dashing =false;
    public SpriteGhostTrailRenderer ghost;
    public GameObject HitBox;
    public AnimationCurve easing;
	bool Moveacces;
	float freeze;
	public Bitcoin RefBitcoin;
	Rigidbody2D rb;
	public GameObject pushzone;
	public Anim2DPlayer animPlayer;
	public bool heroselected;
	public bool heroselected2;
	public bool heroselected3;
	public GameObject Parameter;
	public bool pushing;
	
	// add portrait a changer selon perso;
	public SpriteRenderer Portrait;
	public Sprite hero1;
	public Sprite hero2;
	public Sprite hero3;

    public void Awake() {
        player = ReInput.players.GetPlayer(playerId);
        rb = GetComponent<Rigidbody2D>();
		Moveacces = true;
		freeze = 0;
		animPlayer = GetComponentInChildren<Anim2DPlayer>();
		Parameter = GameObject.Find("/ParameterManager");
		
		if (playerId == 0){
			heroselected = Parameter.GetComponent<ParameterManager>().hero;
			heroselected2 = Parameter.GetComponent<ParameterManager>().hero2;
			heroselected3 = Parameter.GetComponent<ParameterManager>().hero5;
		}
		if (playerId == 1){
			heroselected = Parameter.GetComponent<ParameterManager>().hero3;
			heroselected2 = Parameter.GetComponent<ParameterManager>().hero4;
			heroselected3 = Parameter.GetComponent<ParameterManager>().hero6;
		}
    }

	void Update()
	{
		GetInput();
        ProcessInput();
	}
    public void FixedUpdate () {
        

		if (heroselected == true){
		if ((HitBox.activeSelf) && (HitBox.GetComponent<Hit>().atk == false)){
			animPlayer.SetState(3);
		}else if ((HitBox.activeSelf) && (HitBox.GetComponent<Hit>().atk == true)){
			animPlayer.SetState(2);
		}else if(rb.velocity.magnitude >=0.01f){
			animPlayer.SetState(1);
        } else {
			animPlayer.SetState(0);
		}
		Portrait.sprite = hero1;
		}
		if (heroselected2 == true){
		if ((HitBox.activeSelf) && (HitBox.GetComponent<Hit>().atk == false)){
			animPlayer.SetState(7);
		}else if ((HitBox.activeSelf) && (HitBox.GetComponent<Hit>().atk == true)){
			animPlayer.SetState(6);
		}else if(rb.velocity.magnitude >=0.01f) {
			animPlayer.SetState(5);
        } else {
			animPlayer.SetState(4);
		}
		Portrait.sprite = hero2;
		}
		else if (heroselected3 == true){
		if ((HitBox.activeSelf) && (HitBox.GetComponent<Hit>().atk == false)){
			animPlayer.SetState(11);
		}else if ((HitBox.activeSelf) && (HitBox.GetComponent<Hit>().atk == true)){
			animPlayer.SetState(10);
		}else if(rb.velocity.magnitude >=0.01f){
			animPlayer.SetState(9);
        } else {
			animPlayer.SetState(8);
		}
		Portrait.sprite = hero3;
		}

		if (Moveacces == false){
			freeze += Time.fixedDeltaTime;
			if ( freeze > 0.5f){
				Moveacces = true;
				freeze = 0;
			}
		}
    }

    public void GetInput() {
        move.x = player.GetAxis("Move Horizontal");
        move.y = player.GetAxis("Move Vertical");
        Hit = player.GetButtonDown("Hit");
        Stock = player.GetButtonDown("Stock");
        Dash = player.GetButtonDown("Dash");
    }

	public void ProcessInput() {
		if (Moveacces == true) {
			rb.velocity = (move * Speed * Time.fixedDeltaTime);
		}
        if(Hit && !attaking)
		{
			if(!HitBox.activeSelf) HitBox.SetActive(true);
			atkTimer = 0;
			HitBox.GetComponent<Hit>().atk = true;
			HitBox.GetComponent<Hit>().aim = move;
			attaking = true;
        }
		if(attaking)
		{
			/*atkTimer += Time.fixedDeltaTime;
			if(atkTimer >=0.5)
			{
				atkTimer = 0.5f;
				HitBox.SetActive(false);
				attaking = false;
			}*/
		} 
		if(Stock && !attaking) {
			if(!HitBox.activeSelf) HitBox.SetActive(true);
			atkTimer = 0;
			HitBox.GetComponent<Hit>().atk = false;
			attaking = true;
		}

        if(Dash && !dashing && Moveacces == true)
		{
			posOnStartDash = transform.position;
			posEndDash = transform.position + (Vector3)move.normalized * 3;
			pushzone.layer = 2;
			RaycastHit2D hit = Physics2D.Raycast(transform.position,move,Vector3.Distance(posOnStartDash,posEndDash));
			if(hit)
			{
			posEndDash = hit.point;
			} 
			dashTimer = 0;
			dashing = true;
			Moveacces = false;
			pushzone.layer = 14;
		}
		if(dashing)
		{
			if(!ghost.isActiveAndEnabled)
				ghost.enabled = true;
			dashTimer+= Time.deltaTime* 6;
			transform.position = Vector3.Lerp(posOnStartDash,posEndDash,easing.Evaluate( Mathf.Clamp01( dashTimer) ));
			if(dashTimer>=2f)
			{
				dashTimer = 2f;
				rb.velocity = Vector2.zero;			
				dashing = false;
				ghost.enabled = false;
			}
		}
	}
} 