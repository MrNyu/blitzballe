﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundMenu : MonoBehaviour
{
    public AudioSource select;
    public AudioSource back;
    public AudioSource confirm;
    
    public void Selectsound(){
        select.Play();
    }
    public void Backsound(){
        back.Play();
    }
    public void Comfirmsound(){
        confirm.Play();
    }
}