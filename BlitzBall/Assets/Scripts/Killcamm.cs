﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Killcamm : MonoBehaviour
{
	public Rigidbody2D rigidBody;
    public GameObject bitcoin;
    public float distancefloat;

    void rescaletime(){
        Time.timeScale = 1;
        Debug.Log("killcamend");
    }

    public void KillcamOn() {
        Debug.Log(distancefloat);
        if((bitcoin.GetComponent<Bitcoin>().Bitvalue > 7) &&(bitcoin.GetComponent<Bitcoin>().Bitvalue <= 12)){
            distancefloat = 3.5f;
        }
        if(bitcoin.GetComponent<Bitcoin>().Bitvalue > 12){
            distancefloat = 5f;
        }

    if(Time.timeScale == 1f){
            LayerMask mask = LayerMask.GetMask("Goal");
			RaycastHit2D killcam = Physics2D.Raycast(transform.position, rigidBody.velocity.normalized, distancefloat, mask);
            Debug.DrawRay(transform.position, rigidBody.velocity.normalized, Color.green);
            if(killcam){
                Debug.Log("killcam");
                Time.timeScale = 0.2f;
                Invoke("rescaletime", 0.5f);
            }
        }
    }
}