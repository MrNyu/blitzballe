﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hit : MonoBehaviour {
	public int playerIndex;
	public bool atk;
	public GameObject MainCamera;
	public GameObject Bitcoin;
	public float timer;
	public GameObject speedbarL;
	public GameObject speedbarR;
	public GameObject ultibar1;
	public GameObject ultibar2;
	public Vector2 aim;
	public float stocked;
	float ulti;
	public GameObject player;
	public GameObject player2;
	public float stoppingfor;
	public GameObject chargeball;
	public GameObject stockedanime;
	public GameObject fx;
	public GameObject animationtap;
	public bool hited = false;

	// add sound design de stock et tap;
	public AudioSource SFX;
	public AudioClip tap;
	public AudioClip stock;
	public GameObject me;
	public bool touch = false;


	void notouch(){
		touch = false;
	}

	void Hitedball(){
		hited = false;
	}
	void stopstocked(){
		stockedanime.SetActive(false);
	}
		void stoptap(){
		animationtap.SetActive(false);
	}
	public void Start(){
		hited = false;
		SFX = GameObject.Find("/AudioSource/SFX").GetComponent<AudioSource>();
		me.SetActive(false);
	}

	public void Update(){

			if(touch ==false){
			player.GetComponent<PlayerController>().atkTimer += Time.fixedDeltaTime;
			if(player.GetComponent<PlayerController>().atkTimer >=0.5)
			{
				me.SetActive(false);
				player.GetComponent<PlayerController>().atkTimer = 0f;
				player.GetComponent<PlayerController>().attaking = false;
			}
			}

		if (Bitcoin.GetComponent<Bitcoin>().Bitvalue ==0){
			ultibar1.GetComponent<ultibar>().ResetUlti();
		}
		ulti = player.GetComponent<ultibar>().ulti;
		timer = timer + 1;
		
		if (timer <= 0){
			speedbarL.SetActive(true);
			speedbarR.SetActive(true);
		}
		if ((timer >= (stoppingfor*0.1)) && (timer <= (stoppingfor*0.2))){
			speedbarL.transform.localScale = new Vector3(0.1f, 1,0);
			speedbarR.transform.localScale = new Vector3(0.1f, 1,0);
		}
		if ((timer > (stoppingfor*0.2)) && (timer <= (stoppingfor*0.3))){
			speedbarL.transform.localScale = new Vector3(0.2f, 1,0);
			speedbarR.transform.localScale = new Vector3(0.2f, 1,0);
		}
		if ((timer > (stoppingfor*0.3)) && (timer <= (stoppingfor*0.4))){
			speedbarL.transform.localScale = new Vector3(0.3f, 1,0);
			speedbarR.transform.localScale = new Vector3(0.3f, 1,0);
		}
		if ((timer > (stoppingfor*0.4)) && (timer <= (stoppingfor*0.5))){
			speedbarL.transform.localScale = new Vector3(0.4f, 1,0);
			speedbarR.transform.localScale = new Vector3(0.4f, 1,0);
		}
		if ((timer > (stoppingfor*0.5)) && (timer <= (stoppingfor*0.6))){
			speedbarL.transform.localScale = new Vector3(0.5f, 1,0);
			speedbarR.transform.localScale = new Vector3(0.5f, 1,0);
		}
		if ((timer > (stoppingfor*0.6)) && (timer <= (stoppingfor*0.7))){
			speedbarL.transform.localScale = new Vector3(0.6f, 1,0);
			speedbarR.transform.localScale = new Vector3(0.6f, 1,0);
		}
		if ((timer > (stoppingfor*0.7)) && (timer <= (stoppingfor*0.8))){
			speedbarL.transform.localScale = new Vector3(0.7f, 1,0);
			speedbarR.transform.localScale = new Vector3(0.7f, 1,0);
		}
		if ((timer > (stoppingfor*0.8)) && (timer <= (stoppingfor*0.9))){
			speedbarL.transform.localScale = new Vector3(0.8f, 1,0);
			speedbarR.transform.localScale = new Vector3(0.8f, 1,0);
		}
		if ((timer > (stoppingfor*0.9)) && (timer <= stoppingfor)){
			speedbarL.transform.localScale = new Vector3(0.9f, 1,0);
			speedbarR.transform.localScale = new Vector3(0.9f, 1,0);
		}
		if (timer >=stoppingfor){
			speedbarL.transform.localScale = new Vector3(1,1);
			speedbarR.transform.localScale = new Vector3(1,1);
			player.GetComponent<Rigidbody2D>().velocity = (player.GetComponent<PlayerController>().move * player.GetComponent<PlayerController>().Speed * Time.fixedDeltaTime);
		}

		if(Bitcoin.GetComponent<Bitcoin>().Bitvalue >= 0 && Bitcoin.GetComponent<Bitcoin>().Bitvalue <=4){
		stoppingfor = 10;
		}
		if(Bitcoin.GetComponent<Bitcoin>().Bitvalue >= 5 && Bitcoin.GetComponent<Bitcoin>().Bitvalue <= 8){
		stoppingfor = 20;
		}
		if(Bitcoin.GetComponent<Bitcoin>().Bitvalue >= 9 && Bitcoin.GetComponent<Bitcoin>().Bitvalue <= 11){
		stoppingfor = 45;
		}
		if(Bitcoin.GetComponent<Bitcoin>().Bitvalue >= 12 && Bitcoin.GetComponent<Bitcoin>().Bitvalue <= 14){
		stoppingfor = 75;
		}
		if(Bitcoin.GetComponent<Bitcoin>().Bitvalue >= 15 && Bitcoin.GetComponent<Bitcoin>().Bitvalue <= 250){
		stoppingfor = 100;
		}
		if (timer >= stoppingfor){
			Time.timeScale = 1;
			speedbarL.SetActive(false);
			speedbarR.SetActive(false);
			chargeball.SetActive(false);
		}
	}
	void OnTriggerExit2D(Collider2D other){
		if(touch == true){
		if (other.CompareTag("Bitcoin")){
		me.SetActive(false);
		player.GetComponent<PlayerController>().attaking = false;
		player.GetComponent<PlayerController>().atkTimer =0f;
		}
		if (other.CompareTag("Fastcoin")){
		me.SetActive(false);
		player.GetComponent<PlayerController>().attaking = false;
		player.GetComponent<PlayerController>().atkTimer =0f;
		}
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		speedbarL.SetActive(true);
		speedbarR.SetActive(true);
			if (other.CompareTag("Bitcoin")){
			if(playerIndex == 1) {
				if(Bitcoin.GetComponent<Bitcoin>().Bitvalue >= 1 && Bitcoin.GetComponent<Bitcoin>().Bitvalue <= 3){
					aim.x = 2f;
				}
				if(Bitcoin.GetComponent<Bitcoin>().Bitvalue >= 4 && Bitcoin.GetComponent<Bitcoin>().Bitvalue <= 6){
					aim.x = 4f;
				}
			ultibar1.GetComponent<ultibar>().UpUlti();
		} else if(playerIndex == 2) {
			if(Bitcoin.GetComponent<Bitcoin>().Bitvalue >= 1 && Bitcoin.GetComponent<Bitcoin>().Bitvalue <= 3){
					aim.x = -2f;
				}
				if(Bitcoin.GetComponent<Bitcoin>().Bitvalue >= 4 && Bitcoin.GetComponent<Bitcoin>().Bitvalue <= 6){
					aim.x = -4f;
				}
			ultibar2.GetComponent<ultibar>().UpUlti();
		}
		if((atk) && (hited == false)){
			touch = true;
			Time.timeScale = 0;
			timer = 0;
			if(ulti ==3){
				other.GetComponent<Bitcoin>().Double(playerIndex,aim);
				hited = true;
				//Invoke("Hitedball", 1f);
				animationtap.SetActive(true);
				SFX.clip = tap;
            	SFX.Play();
				Invoke("stoptap", .5f);
				Invoke("notouch", .5f);
				if(playerIndex == 1) {
				ultibar1.GetComponent<ultibar>().ResetUlti();
				} else if(playerIndex == 2) {
				ultibar2.GetComponent<ultibar>().ResetUlti();
				}	
			}
			else {
				other.GetComponent<Bitcoin>().Double(playerIndex,aim);
				hited = true;
				//Invoke("Hitedball", 1f);
				animationtap.SetActive(true);
				SFX.clip = tap;
            	SFX.Play();
				Invoke("stoptap", .5f);
				Invoke("notouch", .5f);
			}
		} else if ((stocked <2) && (hited == false)){
			touch =true;
			other.GetComponent<Bitcoin>().Stock(playerIndex);
			stockedanime.SetActive(true);
			SFX.clip = stock;
            SFX.Play();
			Invoke("stopstocked", 1.4f);
			stocked +=1;
			Invoke("notouch", .5f);
		}
		if(Bitcoin.GetComponent<Bitcoin>().Bitvalue >= 3 && Bitcoin.GetComponent<Bitcoin>().Bitvalue <= 6){
		MainCamera.GetComponent<ScreenShake>().shake();
		}
	}
	if (other.CompareTag("Fastcoin")){
		if(playerIndex == 1) {
			if(Bitcoin.GetComponent<Bitcoin>().Bitvalue >= 7 && Bitcoin.GetComponent<Bitcoin>().Bitvalue <= 9){
					aim.x = 5f;
				}
				if(Bitcoin.GetComponent<Bitcoin>().Bitvalue >= 10 && Bitcoin.GetComponent<Bitcoin>().Bitvalue <= 15){
					aim.x = 9f;
				}
			ultibar1.GetComponent<ultibar>().UpUlti();
		} else if(playerIndex == 2) {
			if(Bitcoin.GetComponent<Bitcoin>().Bitvalue >= 7 && Bitcoin.GetComponent<Bitcoin>().Bitvalue <= 9){
					aim.x = -5f;
				}
				if(Bitcoin.GetComponent<Bitcoin>().Bitvalue >= 10 && Bitcoin.GetComponent<Bitcoin>().Bitvalue <= 15){
					aim.x = -9f;
				}
			ultibar2.GetComponent<ultibar>().UpUlti();
		}
		if((atk) && (hited == false)){
			touch =true;
			Time.timeScale = 0;
			timer = 0;
			MainCamera.GetComponent<ScreenShake>().shake(0.5f,0.2f,10,90);
			chargeball.SetActive(true);
			if(ulti ==3){
				other.GetComponentInParent<Bitcoin>().Double(playerIndex,aim);
				fx.GetComponent<PostFx>().BOOMMUDAFUKAZ(0.5f);
				hited = true;
				//Invoke("Hitedball", 1f);

				animationtap.SetActive(true);
				SFX.clip = tap;
            	SFX.Play();
				Invoke("stoptap", .5f);
				Invoke("notouch", .5f);

				if(playerIndex == 1) {
				ultibar1.GetComponent<ultibar>().ResetUlti();
				} else if(playerIndex == 2) {
				ultibar2.GetComponent<ultibar>().ResetUlti();
				}
			}
			else {
				other.GetComponentInParent<Bitcoin>().Double(playerIndex,aim);
				fx.GetComponent<PostFx>().BOOMMUDAFUKAZ(0.5f);
				hited = true;
				//Invoke("Hitedball", 1f);
				SFX.clip = tap;
            	SFX.Play();
				animationtap.SetActive(true);
				Invoke("stoptap", .5f);
				Invoke("notouch", .5f);
			}
		}
		else if ((stocked <2) && (hited == false)){
			touch =true;
			other.GetComponentInParent<Bitcoin>().Stock(playerIndex);
			stockedanime.SetActive(true);
			SFX.clip = stock;
            SFX.Play();
			Invoke("stopstocked", 1.4f);
			stocked +=1;
			Invoke("notouch", .5f);
		}
		if(Bitcoin.GetComponent<Bitcoin>().Bitvalue >= 7 && Bitcoin.GetComponent<Bitcoin>().Bitvalue <= 15){
		MainCamera.GetComponent<ScreenShake>().shake(0.4f, 0.4f, 10, 90);
		}
		if(Bitcoin.GetComponent<Bitcoin>().Bitvalue >= 15 && Bitcoin.GetComponent<Bitcoin>().Bitvalue <= 250){
		MainCamera.GetComponent<ScreenShake>().shake(0.4f, 0.7f, 10, 90);
		}
	}
}
}