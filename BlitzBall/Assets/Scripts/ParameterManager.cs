﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ParameterManager : MonoBehaviour
{
    public float end = 0;
    public bool Bumper;
    public float goall = 0;
    public float score;
    public float timer;
    public bool hero;
    public bool hero2;
    public bool hero3;
    public bool hero4;
    public bool hero5;
    public bool hero6;

/*  public Sprite Shero;
    public Sprite Shero2;
    public Sprite Shero3;
    public Sprite Shero4;
    public Sprite Shero5;
    public Sprite Shero6;
    public Image selectedhero;
    public Image selectedhero2;
*/
    private static ParameterManager instanceRef;
     
    public void Awake() {
        if(instanceRef == null){
            instanceRef = this;
            DontDestroyOnLoad(gameObject);
        }else {
            DestroyImmediate(gameObject);
            }
    }
    public void bumper(){
        Bumper = true;
    }
    public void nobumper(){
        Bumper = false;
    }
    public void Goal(){
        goall = 1;
    }
    public void Goal2(){
        goall = 2;
    }
    public void End1(){
        end =1;
    }   
    public void End2(){
        end =2;
    }
    public void points1(){
        score = 20;
    }
    public void points2(){
        score = 50;
    }   
    public void points3(){
        score = 100;
    }
    public void points4(){
        score = 200;
    }
    public void heroJ1blu(){
        hero =  true;
        hero2 = false;
        hero5 = false;
        //selectedhero.sprite = Shero2;
    }
    public void heroJ1red(){
        hero2 = true;
        hero = false;
        hero5 = false;
        //selectedhero.sprite = Shero4;
    }   
    public void heroJ2blu(){
        hero3 = true;
        hero4 = false;
        hero6 = false;
        //selectedhero2.sprite = Shero;
    }
    public void heroJ2red(){
        hero4 = true;
        hero3 = false;
        hero6 = false;
        //selectedhero2.sprite = Shero3;
    }
    public void hero3blu(){
        hero = false;
        hero2 = false;
        hero5 = true;
        //selectedhero.sprite = Shero6;
    }
    public void hero3red(){
        hero4 = false;
        hero3 = false;
        hero6 = true;
        //selectedhero2.sprite = Shero5;
    }
    public void time1(){
        timer = 30;
    }
    public void time2(){
        timer = 60;
    }   
    public void time3(){
        timer = 120;
    }
    public void time4(){
        timer = 180;
    }
}