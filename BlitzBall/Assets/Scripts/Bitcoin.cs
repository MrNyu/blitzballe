﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bitcoin : MonoBehaviour 
{
	public But P1But;
	public bool GrowUpStock = false;
	public bool GrowUpDouble = false;
	Vector3 InitialScaleTransform;
	public AnimationCurve bumpCurv;
	public But P2But;
	public int Bitvalue;
	public int Startvalue;
	float BitSpeed;
	int LastPlayer;
	public float hitCount = 0;
	public Vector2 Velocity;
	public Rigidbody2D rigidBody;
	float Progress = 0;
	public float Duration = 1;
	float control = 20;
	public Text ScoreDisplayer;
	public CircleCollider2D slowcoin;
	public CircleCollider2D fastcoin;
	public TrailRenderer ghost;
	public GameObject speedbarL;
	public GameObject speedbarR;
	public GameObject fx;
	//public float swapvector;
	public GameObject killcam;
	public GameObject player1;
	public GameObject player2;
	public GameObject PofWall;
	public GameObject hitbox1;
	public GameObject hitbox2;

	void Start () {
		rigidBody = GetComponent<Rigidbody2D>();
		InitialScaleTransform = transform.localScale;
		Startvalue = Bitvalue;
		Spawn();
		ghost.enabled = false;
	}
	/*void Update(){
		swapvector +=Time.deltaTime * 0.5f;
		
				if ((this.transform.position.x >= -0.5f) && (Bitcoin.transform.position.x <= 0.5f)){
			hited = false;
		}
	}
*/
	public void OnCollisionEnter2D(Collision2D other) {
	if(other.gameObject.CompareTag("Wall")) {
	Instantiate(PofWall, new Vector3(this.transform.position.x,this.transform.position.y,this.transform.position.z), Quaternion.identity);
		}
	if(other.gameObject.CompareTag("bumper")) {
	Instantiate(PofWall, new Vector3(this.transform.position.x,this.transform.position.y,this.transform.position.z), Quaternion.identity);
	hitbox1.GetComponent<Hit>().hited = false;
	hitbox2.GetComponent<Hit>().hited = false;
		}
	}
	
	/*public void OnTriggerEnter2D(Collider2D other){
		if(other.gameObject.CompareTag("filet")) {
		Debug.Log("reset balltap car passé par le filet et nik ta sale race le code");
	}
	}*/
	
	void Update ()
	{	
		if ((this.transform.position.x >= -0.5f) && (this.transform.position.x <= 0.5f)){
			hitbox1.GetComponent<Hit>().hited = false;
			hitbox2.GetComponent<Hit>().hited = false;
		}
		if ( Bitvalue <=2){
			speedbarL.transform.localScale = new Vector3(0.0f, 1.11f);
			speedbarR.transform.localScale = new Vector3(0.0f, 1.11f);
			speedbarL.transform.localPosition = new Vector3(-3.303f, 0f);
			speedbarR.transform.localPosition = new Vector3(3.303f, 0f);
		}
		if (Bitvalue >=2 && Bitvalue <=5){
			speedbarL.transform.localScale = new Vector3(0.3f, 1.11f);
			speedbarR.transform.localScale = new Vector3(0.3f, 1.11f);
			speedbarL.transform.localPosition = new Vector3(-3.242f, 0f);
			speedbarR.transform.localPosition = new Vector3(3.242f, 0f);
		}
		if (Bitvalue >=6 && Bitvalue <=4){
			speedbarL.transform.localScale = new Vector3(0.7f, 1.11f);
			speedbarR.transform.localScale = new Vector3(0.7f, 1.11f);
			speedbarL.transform.localPosition = new Vector3(-3.114f, 0f);
			speedbarR.transform.localPosition = new Vector3(3.114f, 0f);
		}
		if (Bitvalue >=15){
			speedbarL.transform.localScale = new Vector3(1f, 1.11f);
			speedbarR.transform.localScale = new Vector3(1f, 1.11f);
			speedbarL.transform.localPosition = new Vector3(-3.021f, 0f);
			speedbarR.transform.localPosition = new Vector3(3.021f, 0f);
		}
		if ((Bitvalue >= 0) && (Bitvalue <= 6)){
			slowcoin.enabled = true;
			fastcoin.enabled = false;
		}
		if (Bitvalue >= 2){
			ghost.enabled = true;
		}
		if (Bitvalue >= 4){ // règle quand la pièce devient fastcoin
			slowcoin.enabled = false;
			fastcoin.enabled = true;
			killcam.GetComponent<Killcamm>().KillcamOn();
		}
		ScoreDisplayer.text = Bitvalue.ToString();
		if(GrowUpStock == true)
		{
			Progress += Time.deltaTime;
			if(transform.localScale.x == 2)
			{				
			Progress = 1;
			Spawn();
			}
			if(Progress >= 1 )
			{
			 Progress = 0;
			 GrowUpStock = false;
			 Spawn();
			}
			transform.localScale = Vector3.Lerp(InitialScaleTransform, InitialScaleTransform * 2, bumpCurv.Evaluate(Progress * 1/Duration));
		}
		if(GrowUpDouble == true)
		{
			float _currSpeed = rigidBody.velocity.magnitude;
			if(Mathf.Abs(_currSpeed)>=100)
			{
				Progress += Time.deltaTime;
				if(Progress >= 1 )
				{
					Progress = 0;			 
					rigidBody.velocity = Velocity;
					GrowUpDouble = false;
				}
				transform.localScale = Vector3.Lerp(InitialScaleTransform, InitialScaleTransform * 2, bumpCurv.Evaluate(Progress * 1/Duration));
			} else	{
				rigidBody.velocity = Velocity * hitCount;
				GrowUpDouble = false;
			}
		}
	}

	public void Spawn()
	{
		fx.GetComponent<PostFx>().ResetFXState();
		hitCount = 0;
		if(LastPlayer != 0)
		{
			transform.position = new Vector3 (0,15,0);
			if(LastPlayer == 1) rigidBody.velocity = new Vector2(1,-1);
			if(LastPlayer == 2) rigidBody.velocity = new Vector2(-1,1);
		}
		transform.position = new Vector3 (0,0,0);
	}

	public void Stock(int Playerindex)
	{
		if (P1But.Nstock <3){
		rigidBody.velocity = new Vector2 (0,0);
		if(Playerindex == 1) {
			LastPlayer = Playerindex;
			P1But.Nstock ++;
			P1But.ButSizeUpdate();
			P1But.AddBitcoinToBank();	
			GrowUpStock = true;
		}
		}
		if (P2But.Nstock <3){
			rigidBody.velocity = new Vector2 (0,0);
		if(Playerindex == 2)
		{
			LastPlayer = Playerindex;
			P2But.Nstock ++;
			P2But.ButSizeUpdate();
			P2But.AddBitcoinToBank();
			GrowUpStock = true;
		}
		}
	}

	public void Double(int Playerindex, Vector2 aim)
	{
		aim.y = aim.y * control;
		Velocity = aim ;
		rigidBody.velocity = new Vector2 (0,0);
		LastPlayer = Playerindex;
		Bitvalue = Bitvalue +1;
		hitCount += 0.5f;
		GrowUpDouble = true;
	}	
	/*public void Ultimate(int Playerindex, Vector2 aim) {
		swapvector = 0;
		if (swapvector >=0 && swapvector <0.2f){
		aim = new Vector2 (1, 2) * control;
		}
		if (swapvector >=0.2f && swapvector <0.3f){
		aim = new Vector2 (1, -2) * control;
		}
		if (swapvector >=0.3f && swapvector <0.4f){
		aim = new Vector2 (1, 2) * control;
		}
		if (swapvector >=0.4f && swapvector <0.5f){
		aim = new Vector2 (1, -2) * control;
		}
		Velocity = aim;
		rigidBody.velocity = new Vector2 (0, 0);
		LastPlayer = Playerindex;
		Bitvalue = Bitvalue +1;
		hitCount += 0.5f;
		GrowUpDouble = true;
	}*/
}