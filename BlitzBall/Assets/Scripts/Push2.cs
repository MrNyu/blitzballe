﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Push2 : MonoBehaviour
{
    public GameObject player;
    public Rigidbody2D rb;
    Vector2 pushed;
    
    public void Awake(){
        //vecteur trajectoire de la balle
        pushed = rb.velocity.normalized;
    }

	//poussé par la piece rapide
	public void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.CompareTag("Fastcoin"))
        {
            pushed = rb.velocity.normalized*2;
            if( pushed.x >= 0){
            player.transform.position += new Vector3 (pushed.x, pushed.y, 0);
            //other.GetComponentInParent<Bitcoin>().Double(1,Vector2.left);
		}
        }
	}
}