﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreDisplay : MonoBehaviour
{
    public Text ScoreMaxDisplay;
    public Text BankP1;
    public float ScoreP1;
    public float ScoreP2;
    public Text BankP2;
    public Bitcoin RefBitcoin;
    public bool point;
    float ScoreMax;
    public GameObject Parameter;
    public GameObject imgend;
    public Text txtend;

    public void Start(){
        Parameter = GameObject.Find("/ParameterManager");
        imgend.SetActive(false);
        ScoreMax = Parameter.GetComponent<ParameterManager>().score;
    }

	IEnumerator Scenechanges() {
    yield return new WaitForSeconds (1f);
    SceneManager.LoadScene(0);
	}

    public void Update(){
        Display();
        if (point== true) {
        if(ScoreP1 >= ScoreMax){

            StartCoroutine(Scenechanges());
            imgend.SetActive(true);
            txtend.text = "Joueur 1 Win !";
        }
        if(ScoreP2 >= ScoreMax){
            StartCoroutine(Scenechanges());
            imgend.SetActive(true);
            txtend.text = "Joueur 2 Win";
        }
        }
    }
    public void Display(){
        BankP1.text = ScoreP1.ToString();
        BankP2.text = ScoreP2.ToString();
        ScoreMaxDisplay.text = ScoreMax.ToString("0");
    }
    public void UpScoreP1(){
    ScoreP1 += RefBitcoin.gameObject.GetComponent<Bitcoin>().Bitvalue;
    }
    public void UpScoreP2(){
    ScoreP2 += RefBitcoin.gameObject.GetComponent<Bitcoin>().Bitvalue;
    }
}